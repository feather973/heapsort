// basic heap algorithm
// TBD. dynamic expand function

/*
 * 1. BUFSIZE need more than 2
 * 2. heap data can be positive interger only ( 1 ~ MAXINT )
 */
#define MAXINT		65535
#define BUFSIZE		2

/*
 *				buf[1]
 *		buf[2]			buf[3]
 *	buf[4]	buf[5]	buf[6]	buf[7]
 *
 *	idx node can reach parent node idx/2
 */
int buf[BUFSIZE];

int maxidx;
int data[BUFSIZE];

static void upheap(int idx);
static void downheap(int idx);
#include <stdlib.h>
#include <time.h>

// _insert buf[++maxidx]
// upheap
static void _insert(int data)
{
	buf[++maxidx] = data;
	upheap(maxidx);
}

// _remove buf[1]
// buf[1] = buf[maxidx--]
// downheap
static int _remove(void)
{
	int res;

	res = buf[1];					// always return buf[1]
	buf[1] = buf[maxidx--];
	downheap(1);

	return res;
}

static void upheap(int idx)
{
	int tmp;

	tmp = buf[idx];
	buf[0] = MAXINT;				// temporary value for loop guard

	// if parent is small
	while (buf[idx/2] <= tmp) {
		buf[idx] = buf[idx/2];		// parent value switch to current
		idx = idx / 2;				// current move to former parent
	}

	// parent is large
	buf[idx] = tmp;
}

static void downheap(int idx)		// index that tmp will be assigned
{
	int tmp;
	tmp = buf[idx];			// saved

	int i;		// temporary index for check left/right child

	// check child need <= maxidx / 2
	while (idx <= maxidx / 2) {
		// goes left child
		i = idx * 2;

		// if left < right, check right child instead
		if (i < maxidx && buf[i] < buf[i+1])
			i++;

		// check child ( idx is i's parent )
		if (tmp >= buf[i])
			break;

		// child goes to parent
		buf[idx] = buf[i];
		idx = i;
	}

	// add to parent
	buf[idx] = tmp;
}

void heapsort(void)
{
	int i;

	for (i=0; i<BUFSIZE; i++)
		_insert(data[i]);

	for (i=BUFSIZE-1;i>=0;i--)
		data[i] = _remove();
}

void display(void)
{
	int i;

	for (i=0;i<BUFSIZE;i++) {
		if (i%10 == 0)
			printf("\n");

		printf("%4d ", data[i]);
	}

	printf("\n");
}

void initdata(void)
{
	int i = 0;
	int num;
	srand( (unsigned)time(NULL) );

	while (i < BUFSIZE) {
		num = rand() % MAXINT;
		data[i] = num;
		i++;
	}
}

int main()
{
	maxidx = 0;
	initdata();

	printf("before\n");
	display();

	printf("====\n");
	heapsort();

	printf("after\n");
	display();
}
